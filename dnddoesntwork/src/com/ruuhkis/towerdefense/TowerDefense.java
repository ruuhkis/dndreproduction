package com.ruuhkis.towerdefense;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.ruuhkis.towerdefense.editor.DNDScreen;
import com.ruuhkis.towerdefense.tower.TowerDef;


public class TowerDefense implements ApplicationListener {

	private Screen currentScreen;
	
	
	
	private TextureAtlas atlas;
	

	private TowerDef towerDef;
	
	@Override
	public void create() {	
		Gdx.app.setLogLevel(Application.LOG_DEBUG);	
		atlas = new TextureAtlas("data/sprites.atlas");
		loadTowerDef();
		
		
		currentScreen = new DNDScreen(towerDef, atlas);
	
	}

	private void loadTowerDef() {
		towerDef = new TowerDef(atlas);
	}



	@Override
	public void dispose() {
		atlas.dispose();
		currentScreen.dispose();
	}

	@Override
	public void render() {		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		float deltaTime = Gdx.graphics.getDeltaTime();
		currentScreen.render(deltaTime);
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	
	
}
