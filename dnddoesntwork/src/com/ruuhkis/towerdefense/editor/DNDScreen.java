package com.ruuhkis.towerdefense.editor;

import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Payload;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Source;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Target;
import com.esotericsoftware.tablelayout.BaseTableLayout.Debug;
import com.ruuhkis.towerdefense.editor.windows.TowerBuildingWindow;
import com.ruuhkis.towerdefense.tower.TowerDef;

public class DNDScreen implements Screen {
	
	
	
	private Stage stage;
	private Skin skin;

	private TowerBuildingWindow towerWindow;
	private TowerDef towerDef;
	
	public DNDScreen(TowerDef towerDef, TextureAtlas atlas) {
		this.towerDef = towerDef;
		
		
		
		stage = new Stage();
		
		skin = new Skin();
		FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("data/font.ttf"));
			
		
		BitmapFont bitmapFont = gen.generateFont(30);
		bitmapFont.setColor(1f, 0f, 0f, 1f);
		skin.add("default-font", bitmapFont);
		
		skin.addRegions(atlas);
		skin.load(Gdx.files.internal("data/sprites.json"));
		
		Gdx.input.setInputProcessor(new InputMultiplexer(stage));
		createHUD();
		

		
//		Gdx.app.log("MapEditorScreen", "Type: " + stage.getRoot().getClass() + " - " + (stage.getRoot() instanceof Table));
	}

	private void createHUD() {
		createTowerWindow();
	}

	
	private void createTowerWindow() {
		towerWindow = new TowerBuildingWindow(skin, towerDef);
		towerWindow.setPosition(100, 100);
		stage.addActor(towerWindow);
		
		towerWindow.getDnd().addTarget(new Target(stage.getRoot()) {
			
			

			@Override
			public boolean drag(Source source, Payload payload, float x,
					float y, int pointer) {
				getActor().setColor(Color.GREEN);
				return true;
			}
			
			public void reset (Source source, Payload payload) {
				getActor().setColor(Color.WHITE);
			}

			@Override
			public void drop(Source source, Payload payload, float x, float y,
					int pointer) {
				Gdx.app.log("MapEditorScreen", "Dropped!");
			}
			
		});
		
		table = new Table();
		table.setFillParent(true);
		table.debug(Debug.all);
		Label label = new Label("Test", skin);
		
//		label.setFillParent(true);
		table.add(label);
		label.setTouchable(Touchable.disabled);
		towerWindow.getDnd().addTarget(new Target(label) {
			
			

			@Override
			public boolean drag(Source source, Payload payload, float x,
					float y, int pointer) {
				getActor().setColor(Color.GREEN);
				return true;
			}
			
			public void reset (Source source, Payload payload) {
				getActor().setColor(Color.WHITE);
			}

			@Override
			public void drop(Source source, Payload payload, float x, float y,
					int pointer) {
				Gdx.app.log("MapEditorScreen", "Dropped!");
			}
			
		});
		towerWindow.getDnd().addTarget(new Target(table) {
			
			

			@Override
			public boolean drag(Source source, Payload payload, float x,
					float y, int pointer) {
				getActor().setColor(Color.GREEN);
				return true;
			}
			
			public void reset (Source source, Payload payload) {
				getActor().setColor(Color.WHITE);
			}

			@Override
			public void drop(Source source, Payload payload, float x, float y,
					int pointer) {
				Gdx.app.log("MapEditorScreen", "Dropped!");
			}
			
		});
		
		
		stage.addActor(table);
	}

	private float totalTime = 0f;
	
	private final float CYCLE_TIME = 1000f / 20f / 1000f;
	private Table table;
	
	@Override
	public void render(float delta) {
		
		stage.act(delta);
		stage.draw();
		
		Table.drawDebug(stage);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
