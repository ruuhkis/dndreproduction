package com.ruuhkis.towerdefense.editor.windows;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Payload;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Source;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Target;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.ruuhkis.towerdefense.entity.EntityGraphics;
import com.ruuhkis.towerdefense.tower.TowerDef;
import com.ruuhkis.towerdefense.tower.TowerInfo;

public class TowerBuildingWindow extends Window {
	
	private int counter;
	private Table towerTable;
	
	private Skin skin;
	private TowerDef def;
	
	private DragAndDrop dnd;
	

	public TowerBuildingWindow(Skin skin, TowerDef def) {
		super("Tower building", skin);
		this.skin = skin;
		this.def = def;
		this.dnd = new DragAndDrop();
		createWindow();
	}

	private void createWindow() {
		towerTable = new Table();
		
		
		addTowers();
		
		add(new ScrollPane(towerTable, skin));
		
		row();
		
		
		pack();
	}


	public void addTower(TowerInfo info) {
		addTower(def.getTowerGraphicsForInfo(info)); 
	}
	
	private void addTower(final EntityGraphics<TowerInfo> tileGraphics) {
		
		TextureRegionDrawable drawable = new TextureRegionDrawable(tileGraphics.getRegion());
		ImageButton button = new ImageButton(drawable, drawable);
		button.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				
			}
			
		});
		
		
		dnd.addSource(new Source(button) {

			@Override
			public Payload dragStart(InputEvent event, float x, float y,
					int pointer) {
				Payload payload = new Payload();
				payload.setObject("Some payload!");
				

				payload.setDragActor(new Label("Some payload!", skin));

				Label validLabel = new Label("Some payload!", skin);
				validLabel.setColor(0, 1, 0, 1);
				payload.setValidDragActor(validLabel);

				Label invalidLabel = new Label("Some payload!", skin);
				invalidLabel.setColor(1, 0, 0, 1);
				payload.setInvalidDragActor(invalidLabel);

				return payload;
			}
			
		});
		
		
		
		
		
		towerTable.add(button).pad(2f);
		if(counter++ > 0 && counter % 4 == 0) {
			towerTable.row();
		}
	}
	
	private void addTowers() {
		
		for(EntityGraphics<TowerInfo> tileGraphics: def.getTowerGraphics()) {
			addTower(tileGraphics);
		}
		dnd.addTarget(new Target(towerTable) {

			@Override
			public boolean drag(Source source, Payload payload, float x,
					float y, int pointer) {
				return true;
			}

			@Override
			public void drop(Source source, Payload payload, float x, float y,
					int pointer) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		dnd.addTarget(new Target(this) {

			@Override
			public boolean drag(Source source, Payload payload, float x,
					float y, int pointer) {
				return true;
			}

			@Override
			public void drop(Source source, Payload payload, float x, float y,
					int pointer) {
				// TODO Auto-generated method stub
				
			}
			
		});
		towerTable.pack();
		towerTable.invalidate();
	}

	public DragAndDrop getDnd() {
		return dnd;
	}

	
	
}