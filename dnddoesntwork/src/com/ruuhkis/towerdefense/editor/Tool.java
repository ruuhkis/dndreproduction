package com.ruuhkis.towerdefense.editor;

public enum Tool {
	BRUSH("pencil"), START("start"), END("end"), GENERATE_PATH("gen", true);
	
	private String imageIdentifier;
	private boolean clickable;
	
	Tool(String imageIdentifier) {
		this.imageIdentifier = imageIdentifier;
	}
	
	Tool(String imageIdentifier, boolean clickable) {
		this(imageIdentifier);
		this.clickable = clickable;
	}

	public String getImageIdentifier() {
		return imageIdentifier;
	}

	public boolean isClickable() {
		return clickable;
	}
	
	
	
}
