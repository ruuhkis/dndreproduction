package com.ruuhkis.towerdefense.tower.shot;

public class Shot {

	private ShotInfo info;

	public Shot(ShotInfo info) {
		super();
		this.info = info;
	}

	public ShotInfo getInfo() {
		return info;
	}
	
	
	
}
