package com.ruuhkis.towerdefense.tower.shot;

public class ShotInfo {

	private String imageIdentifier;
	private int damage;
	
	public String getImageIdentifier() {
		return imageIdentifier;
	}
	
	public int getDamage() {
		return damage;
	}
	
	
	
}
