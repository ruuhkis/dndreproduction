package com.ruuhkis.towerdefense.tower;

import com.ruuhkis.towerdefense.tower.shot.ShotInfo;

public class TowerInfo {

	private String imageIdentifier;
	private ShotInfo shot;
	private int id;
	private int maxDistance;
	
	public TowerInfo(String imageIdentifier, int id) {
		this.imageIdentifier = imageIdentifier;
		this.id = id;
	}

	public String getImageIdentifier() {
		return imageIdentifier;
	}
	
	public ShotInfo getShot() {
		return shot;
	}

	public int getMaxDistance() {
		return maxDistance;
	}

	public int getId() {
		return id;
	}

	public void setImageIdentifier(String imageIdentifier) {
		this.imageIdentifier = imageIdentifier;
	}

	public void setShot(ShotInfo shot) {
		this.shot = shot;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setMaxDistance(int maxDistance) {
		this.maxDistance = maxDistance;
	}
	
	
	
}
