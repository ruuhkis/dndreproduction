package com.ruuhkis.towerdefense.tower;

import com.ruuhkis.towerdefense.entity.EntityGraphics;


public class Tower {

	private TowerInfo info;
	private EntityGraphics<TowerInfo> graphics;

	public Tower(TowerInfo info, EntityGraphics<TowerInfo> graphics) {
		super();
		this.info = info;
		this.graphics = graphics;
	}

	public TowerInfo getInfo() {
		return info;
	}

	public EntityGraphics<TowerInfo> getGraphics() {
		return graphics;
	}
	
	
	
}
