package com.ruuhkis.towerdefense.tower;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.ruuhkis.towerdefense.entity.EntityGraphics;
import com.thoughtworks.xstream.XStream;

public class TowerDef {

	private List<TowerInfo> towerInfos;
	private List<EntityGraphics<TowerInfo>> towerGraphics;
	private XStream xstream;
	private TextureAtlas atlas;
	
	public TowerDef(TextureAtlas atlas) {
		this.towerGraphics = new ArrayList<EntityGraphics<TowerInfo>>();
		this.atlas = atlas;
		towerInfos = new ArrayList<TowerInfo>();
		towerInfos.add(new TowerInfo("tower", 0));
		preloadTowers();
	}
	
	private void preloadTowers() {
		towerGraphics.clear();
		for(TowerInfo info: towerInfos) {
			AtlasRegion region = atlas.findRegion(info.getImageIdentifier());
			towerGraphics.add(new EntityGraphics<TowerInfo>(info, region));
		}
	}
	
	public EntityGraphics<TowerInfo> getTowerGraphicsForInfo(TowerInfo info) {
		for(EntityGraphics<TowerInfo> towerGraphic: towerGraphics) {
			if(towerGraphic.getInfo().getId() == info.getId())
				return towerGraphic;
		}
		return new EntityGraphics<TowerInfo>(info, atlas.findRegion("question"));
	}
	
	public TowerInfo getTowerInfoForId(int id) {
		for(TowerInfo info: towerInfos) {
			if(info.getId() == id)
				return info;
		}
		return null;
	}
	
	public TowerInfo createNewTower() {
		int id = getUnusedId();
		TowerInfo info = new TowerInfo("question", id);
		return info;
	}

	private int getUnusedId() {
		for(int i = 0; i < Integer.MAX_VALUE; i++) {
			if(getTowerInfoForId(i) == null)
				return i;
		}
		return -1;
	}


	public List<EntityGraphics<TowerInfo>> getTowerGraphics() {
		return towerGraphics;
	}

	public void saveData() {
		FileHandle towerHandle = Gdx.files.local("towers.xml");
		Writer writer = towerHandle.writer(false);
		Gdx.app.debug("TowerDef", "Writing to file " + towerHandle.name());
		xstream.toXML(towerInfos, writer);
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void registerTower(TowerInfo info) {
		towerInfos.add(info);
		towerGraphics.add(new EntityGraphics<TowerInfo>(info, atlas.findRegion(info.getImageIdentifier())));
	}
	
	
	
}
