package com.ruuhkis.towerdefense.entity;

import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;

public class EntityGraphics<T> {

	private T info;
	private AtlasRegion region;

	public EntityGraphics(T info, AtlasRegion region) {
		this.info = info;
		this.region = region;
	}

	public AtlasRegion getRegion() {
		return region;
	}

	public T getInfo() {
		return info;
	}
	
}
